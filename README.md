# Statmon

Statmon is a small and lightweight, server monitoring solution designed to be easy to use from the ground up.

### No client needed!
Yeap, That's right! A small and lightweight simple to install and keep
maintained remote server monitoring solution. With easy to develop modules.

## Desktop and Mobile Friendly

![](DOCS/demo.png)

# Dependencies
This program requires the following to be installed:
- Apache Webserver
- PHP
- sshpass (for remote logins)

# Contribution Rules
- 4 Spaces - NOT tabs... (please)
- 80 char width soft limit (it just looks nice)
    
# Code of Conduct
- Be excellent to each other! (it's the LAW, so deal with it.)

# News
A new 'testing area' has been created in the testing folder. This area should be
used for developing new ideas and concepts for the mainstream release of 
Statmon.