<?php

    // Server Name: Test Server
    $ServerName = "Test Server";
    $ServerOS = "Linux";
    
    // SSH Login Creds
    $ServerIP = "localhost";
    $SSHUser = "testuser";
    $SSHPass = "testpassword";

    // Ping a port
    $CheckPorts = array(80, 22, 1000); // Test Ports in array

?>