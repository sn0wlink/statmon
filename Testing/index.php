<?php

################################################################################

//  -=[ STATMON ]=-
//
//  Flatfile Remote System Monitor
//  Written by: David Collins-Cubitt (Sn0wlink 2020)
//
//  https://www.sn0wlink.com
//  https://www.gitlab.com/sn0wlink

################################################################################

echo "
<html>
    <head>
    <title>Statmon</title>
    <link rel='icon' type='image/png' href='images/logo-icon-blue.png'>
    </head>

<style>
body {
    color: #ADA8A6;
    background-color: #222121;
    font-family: atlanta;
    font-size: 11pt;
}

hr {
    border: 1px solid #ADA8A6;
}

div.menu-header {
    color: #F7F5FB;
    background-color: #063576;
    font-size: 20pt;
    position: fixed;
    width: 100%;
    height: 35px;
    left: 0;
    top: 0;
    padding: 8px;
}

div.menu-description {
    color: #ffffff;
    font-size: 12pt;
    position: fixed;
    top: 15px;
    right: 15px;
}

div.content {
    padding: 50 10 40 10;
}

div.Server-Block {
    width: 400px;
    display: inline-block;
    vertical-align: top;
    padding: 20px;
}

div.Server-Header{
    font-size:20pt;
}

div.ping {
    text-align: right;
}

div.ping-red {
    color: #FF042C;
    text-align: right;
}

div.footer {
    color: #ADA8A6;
    background-color: #151515;
    font-size: 9pt;
    position: fixed;
    text-align: center;
    width: 100%;
    left: 0;
    bottom: 0;
    padding: 8px;

}
</style>

<body>

<div class='menu-header'>
<img src='images/logo.png' alt='Logo'>
STATMON
</div>

<div class='menu-description'>
Clustered server monitoring solutions
</div>

<div class='content'>
<div class='Center-Content'>
";

  //////////////////////////////
 //  SCROLL THROUGH SERVERS  //
//////////////////////////////

foreach(glob("servers/*") as $filename) {

    include ($filename);

    // Setup a server block
    echo "<div class='Server-Block'>";

    echo "<div class='Server-Header'>$ServerName</div>";
    echo "<hr />";

    // Print SSH Command
    PrintOS($ServerIP, $SSHUser, $SSHPass, $SSHKey);
    
    // Print the usuage bar graphs
    CpuUsage($ServerIP, $SSHUser, $SSHPass);
    MemUsage($ServerIP, $SSHUser, $SSHPass);
    DiskUsage($ServerIP, $SSHUser, $SSHPass);

    // Ping Ports
    PingIt($CheckPorts);

    // Cleanup Variables
    unset($ServerName);
    unset($ServerOS);
    unset($ServerIP);
    unset($SSHUser);
    unset($SSHPass);
    unset($CheckPorts);

    echo "</div>";
}

  ////////////////////
 //  PRINT FOOTER  //
////////////////////

echo "
</div>
<div class='footer'>
Designed and created by David Collins-Cubitt
</div>
</body>
</html>
";

  /////////////////
 //  FUNCTIONS  //
/////////////////

// Print OS Type
function PrintOS($ServIP, $User, $Pass, $SSHKey) {
    
    $SSHLogin = "sshpass -p $Pass ssh -o StrictHostKeyChecking=no $User@$ServIP";
    $MachineName = shell_exec("$SSHLogin 'cat /etc/os-release'");

    // Cleanup Machine Name
    $MachineName = explode ("\n" , $MachineName);
    $MachineName = $MachineName[0];
    $MachineName = trim( $MachineName, "PRETTY_NAME=");
    $MachineName = trim( $MachineName, '"');

    echo "OS: $MachineName <br />";
}

function CpuUsage($ServIP, $User, $Pass) {
    $Command = "grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'";
    
    $SSHLogin = "sshpass -p $Pass ssh -o StrictHostKeyChecking=no $User@$ServIP";
    $Usage = shell_exec("$SSHLogin $Command");

    // Print as Graph
    Grafit('CPU Usage', $Usage, 100);
}

function MemUsage($ServIP, $User, $Pass) {
    $Command = "cat /proc/meminfo";

    $SSHLogin = "sshpass -p $Pass ssh -o StrictHostKeyChecking=no $User@$ServIP";
    $Usage = shell_exec("$SSHLogin $Command");

    // Clean up
    $Usage = preg_replace('!\s+!', '¬', $Usage);
    $Usage = explode('¬', $Usage);
    
    // $Usage = explode("\n\r", $Usage);
    $Free = $Usage[7];
    $Total = $Usage[1];
    $Used = $Total - $Free;

    // Print as Graph
    Grafit('Memory Usage', $Used, $Total);
}

function DiskUsage($ServIP, $User, $Pass) {
    $Command = "df /";

    $SSHLogin = "sshpass -p $Pass ssh -o StrictHostKeyChecking=no $User@$ServIP";
    $Usage = shell_exec("$SSHLogin $Command");

    // Clean up
    $Usage = preg_replace('!\s+!', '¬', $Usage);
    $Usage = explode('¬', $Usage);
    
    // $Usage = explode("\n\r", $Usage);
    $Used = $Usage[11];
    $Used = trim($Used, '%');

    // Print as Graph
    Grafit('Disk Usage', $Used, 100);
}

// Pings the port, to see if your... Still Alive ->
function PingIt($Port) {

    foreach ($Port as $PortNum) {

        $PingIt = fsockopen(localhost, $PortNum);

        if ($PingIt) {
            echo "
            <div class='ping'>
            Port $PortNum: Online
            </div>";
        }

        else {
            echo "
            <div class='ping-red'>
            Port $PortName: No Connection
            </div>";
        }

    }
}   

// Bar Graph Generator
function Grafit ($Name, $value,$total) {

    $barwidth = 200;
    $barheight = 18;

    // Calculates bargraph dimensions
    $ratio = $barwidth/$total;
    $barvaluewidth = $ratio*$value;

    // Calculate percentage for else statement
    $Percentage = (100/$total)*$value;
    $Percentage = round($Percentage); // Fix the float

    $barcolour = "#D2FF1A"; // Set to yellow

    if ($Percentage > 25) {
        $barcolour = "#FFC52C"; // Red
    }

    if ($Percentage > 50) {
        $barcolour = "#FF7D2C"; // Red
    }

    // Change bar colour depending on value
    if ($Percentage > 75) {
        $barcolour = "#FF042C"; // Red
    }

    echo "
        <div 
            style='
            display: inline-block;
            text-align: right;
            padding: 0;
            width: 150px;'>
            $Name
        </div>

        <div style='
            display:inline-block;
            height: $barheight;
            width: $barwidth;'>
                <div style='
                    background-color: $barcolour;
                    height: $barheight;
                    width: $barvaluewidth;'>
                </div>
        </div>

        <div style='display: inline-block;'>$Percentage %</div>
        <br />
    ";
}

?>